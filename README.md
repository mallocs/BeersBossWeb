<h1 align="center">Beers Boss</h1>

<p align="center">
  <img src="https://beersboss.com/assets/beerCanIcon.png" alt="beers boss" width="120px" height="120px"/>
  Scan a beer's UPC and find out who owns it.
</p>
